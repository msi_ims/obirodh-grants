<?php

// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/dotenv-loader.php';

use Auth0\SDK\Auth0;

$domain        = getenv('AUTH0_DOMAIN');
$client_id     = getenv('AUTH0_CLIENT_ID');
$client_secret = getenv('AUTH0_CLIENT_SECRET');
$redirect_uri  = getenv('AUTH0_CALLBACK_URL');
$audience      = getenv('AUTH0_AUDIENCE');

if($audience == ''){
  $audience = 'https://' . $domain . '/userinfo';
}

$auth0 = new Auth0([
  'domain' => $domain,
  'client_id' => $client_id,
  'client_secret' => $client_secret,
  'redirect_uri' => $redirect_uri,
  'audience' => $audience,
  'scope' => 'openid profile',
  'persist_id_token' => true,
  'persist_access_token' => true,
  'persist_refresh_token' => true,
]);

$userInfo = $auth0->getUser();


?>
<?php if(!$userInfo): ?>
<html>
<head>
  <style>
  @page {
      size: auto !important;

  }
  body.home {
    background-color: #343a40;
  }
  div.login-box {
    background-color: #343a40;
  }
  div.container{
    background-color: #343a40;
  }
</style>
  <script src="https://code.jquery.com/jquery-3.1.0.min.js" type="text/javascript"></script>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- font awesome from BootstrapCDN -->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

  <link href="public/app.css" rel="stylesheet">



</head>
<body class="home">
  <div class="container">
    <div class="login-page clearfix">

        <div class="login-box auth0-box before">
          <h2 style="color: white;">Obirodh Grants Reports User Authentication</h2>
          <a id="qsLoginBtn" class="btn btn-primary btn-lg btn-login btn-block" style="width: 70%; margin: 0 auto !important;" href="login.php">Sign In</a>
          <br>
          <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9c/%E0%A6%9C%E0%A6%BE%E0%A6%A4%E0%A7%80%E0%A6%AF%E0%A6%BC_%E0%A6%B8%E0%A7%8D%E0%A6%AE%E0%A7%83%E0%A6%A4%E0%A6%BF_%E0%A6%B8%E0%A7%8C%E0%A6%A7_-_The_National_Martyrs%27_Monument_of_Bangladesh.jpg/1200px-%E0%A6%9C%E0%A6%BE%E0%A6%A4%E0%A7%80%E0%A6%AF%E0%A6%BC_%E0%A6%B8%E0%A7%8D%E0%A6%AE%E0%A7%83%E0%A6%A4%E0%A6%BF_%E0%A6%B8%E0%A7%8C%E0%A6%A7_-_The_National_Martyrs%27_Monument_of_Bangladesh.jpg" />
        </div>
      </div>
    </div>
  </body>
  </html>
<?php else: ?>


  <?php include_once('home.html'); ?>

<?php endif ?>
